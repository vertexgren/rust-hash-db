use axum::{extract::State, response::IntoResponse, Json};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::{collections::HashMap, time::Instant};
use tracing::info;

use crate::utils::state::{append_to_json_file, AppState};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct KeyValueInsert {
    pub key: String,
    pub value: Value,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct KeyValueGet {
    pub key: String,
}

// post take json and return Status code
pub async fn insert_key_value(
    State(state): State<AppState>,
    Json(key_value): Json<KeyValueInsert>,
) -> impl IntoResponse {
    let start_time = Instant::now(); // Start time measurement
    info!("Inserting key value: {:?}", key_value);

    let mut key_value_map = state.key_value.lock().await;

    let result = key_value_map.insert(key_value.key.clone(), key_value.value.clone());

    return match result {
        Some(value) => {
            let mut hash_map_key_value = HashMap::new();
            hash_map_key_value.insert(key_value.key.clone(), key_value.value.clone());

            info!("Key value updated: {:?}", key_value);

            (axum::http::StatusCode::CREATED, "Key value updated")
        }
        None => {
            info!("Key value inserted: {:?}", key_value);
            append_to_json_file(&key_value.key, &key_value.value)
                .await
                .unwrap();
            let elapsed_time = start_time.elapsed().as_secs_f64();

            println!("Execution time: {:.6} seconds", elapsed_time);
            (axum::http::StatusCode::CREATED, "Key value inserted")
        }
    };
}

pub async fn get_value_by_key(
    State(state): State<AppState>,
    Json(key): Json<KeyValueGet>,
) -> impl IntoResponse {
    let start_time = Instant::now(); // Start time measurement
    info!("Getting value for key: {:?}", key);

    let key_value_map = state.key_value.lock().await;

    let value = key_value_map.get(&key.key);

    let elapsed_time = start_time.elapsed().as_secs_f64(); // Calculate elapsed time

    println!("Execution time: {:.6} seconds", elapsed_time);
    return match value {
        Some(value) => {
            info!("Value found for key: {:?}", key);
            (axum::http::StatusCode::OK, Json(value.to_owned()))
        }
        None => {
            info!("Value not found for key: {:?}", key);
            (
                axum::http::StatusCode::NOT_FOUND,
                Json(json!({ "error": "Key not found" })),
            )
        }
    };
}

pub async fn get_all_key_value(State(state): State<AppState>) -> impl IntoResponse {
    let start_time = Instant::now(); // Start time measurement

    let key_value_map = state.key_value.lock().await;

    let elapsed_time = start_time.elapsed().as_secs_f64(); // Calculate elapsed time

    println!("Execution time: {:.6} seconds", elapsed_time);

    (axum::http::StatusCode::OK, Json(key_value_map.clone()))
}
