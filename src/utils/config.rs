use axum::http::HeaderValue;
use clap::Parser;
use tower_http::cors::AllowOrigin;

#[derive(Parser, Debug)]
#[clap(name = "Rust hash db config", about = "DATABASE", version = "1.0")]
pub struct Config {
    /// Server configuration
    #[clap(flatten)]
    pub server: Server,
}

#[derive(Parser, Debug)]
pub struct Server {
    /// Server host
    #[clap(long, env)]
    pub rest_host: String,

    /// Server port
    #[clap(long, env)]
    pub rest_port: String,
}
