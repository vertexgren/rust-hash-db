use ahash::{AHasher, RandomState};
use serde::Serialize;
use serde_json::ser::PrettyFormatter;
use serde_json::{json, Deserializer, Map, Serializer, Value};
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::io::Write;
use std::sync::Arc;

use crate::routes::KeyValueInsert;

use super::config::Config;
use serde_json::to_writer_pretty;

use tokio::sync::Mutex;
use tracing::info;

pub type KeyValueMap = HashMap<String, Value, RandomState>;

#[derive(Clone)]
/// The data that is shared across the processes.
pub struct AppState {
    pub key_value: Arc<Mutex<KeyValueMap>>,
}

impl AppState {
    pub async fn init(config: &Config) -> Self {
        // let key_value = Arc::new(Mutex::new(HashMap::new()));

        let file = std::fs::File::open("src/Dumb.json");
        let key_value_map_init = match file {
            Ok(file) => {
                let reader = std::io::BufReader::new(file);
                let key_value_map_from_dumb: HashMap<String, Value, RandomState> =
                    serde_json::from_reader(reader).unwrap();

                key_value_map_from_dumb
                // let mut key_value_map = key_value.lock().await;
            }
            Err(_) => {
                info!("File not found, creating a new file");
                let key_value_map = HashMap::default();

                key_value_map
            }
        };

        let key_value_map = Arc::new(Mutex::new(key_value_map_init));

        AppState {
            key_value: key_value_map,
        }
    }
}

pub async fn append_to_json_file(key: &str, value: &Value) -> std::io::Result<()> {
    let file = OpenOptions::new()
        .read(true)
        .write(true)
        .create(true)
        .open("src/Dumb.json")?;
    let mut json_data: Value = if let Ok(mut file) = file.try_clone() {
        serde_json::from_reader(&mut file)?
    } else {
        json!(Map::new())
    };
    json_data[key] = value.clone();
    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true) 
        .open("src/Dumb.json")?;
    file.write_all(serde_json::to_string_pretty(&json_data)?.as_bytes())?;

    Ok(())
}
