use axum::{
    routing::{get, post},
    Router,
};

use clap::Parser;
use tower_http::cors::{Any, CorsLayer};
use tracing::info;
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};
use utils::config::{self};

use crate::{
    routes::{get_all_key_value, get_value_by_key, insert_key_value},
    utils::state::AppState,
};

mod routes;
mod utils;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();

    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env()
                .unwrap_or_else(|_| "debug".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let config = config::Config::parse();

    let cors = CorsLayer::new()
        .allow_origin(Any)
        .allow_methods(Any)
        .allow_headers(Any)
        .allow_credentials(false);

    let state = AppState::init(&config).await;

    let app = Router::new()      
        .route("/api/v0/insert", post(insert_key_value))
        .route("/api/v0/get", post(get_value_by_key))
        .route("/api/v0/list", get(get_all_key_value))
        .with_state(state)
        .layer(cors);

    let server = format!("{}:{}", config.server.rest_host, config.server.rest_port);

    info!("Server started at: {}", server);
    let listener = tokio::net::TcpListener::bind(server).await.unwrap();

    axum::serve(listener, app).await.unwrap();
}
